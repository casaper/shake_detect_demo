package li.tor.droid.shakeit;

/**
 * Created by kaspi on 05.02.18.
 */

public interface AccelerometerListener {

    public void onAccelerationChanged(float x, float y, float z);

    public void onShake(float force);
}
