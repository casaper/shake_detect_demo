# Schütteln erkennen

## Technologie

Schütteln wird mittels des **Accelerometer** des Handys erkannt. Erkennt das Telefon eine schnelle bewegung, schnell aufeinander folgend und jeweils in die entgegengesetzte Richtung der Bewegung die der jetztigen vorangegangen war, handelt es sich um ein Schütteln.

### Online referenzen

[Wikipedia - Beschleunigungsensor](https://de.wikipedia.org/wiki/Beschleunigungssensor)

## Erkennen von Schütteln in einer Android app

### Berechtigung

Als erstes müssen wir die Applikaiton dazu bringen die Berechtigung für den Sensor vom Telefon zu verlangen.

In der Datei `app/src/main/AndroidManifest.xml`:

```xml
<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```

Um den Zugriff auf den Sensor zu ermöglichen muss ein Listener erzeugt werden.

```java

    public static void startListening(AccelerometerListener accelerometerListener) {
		// context kommt von AppCompatActivity -> der haupt klasse
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        // Hole alle Accellerometer des Telefons
        List<Sensor> sensors = sensorManager.getSensorList(
                Sensor.TYPE_ACCELEROMETER);

      	// Wenn Ein Sensor vorhanden ist
        if (sensors.size() > 0) {
			// Normalerweise hat ein Telefon nur einen Accelerometer
            sensor = sensors.get(0);

            // Den Listener fuer den Accelerometer hinzufuegen
            running = sensorManager.registerListener(
                    sensorEventListener, // Dies ist der Kern der Sache
              		sensor,
                    SensorManager.SENSOR_DELAY_GAME
            );

            listener = accelerometerListener;
        }
    }
```



Der [sensorEventListener](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/java/li/tor/droid/shakeit/AccelerometerManager.java#L134) ist der Kern der Sache, und, wenn man bedenkt, erstaundlich kompliziert.

Da dieser Code Viel zu lange wäre, hier die kompletten code Beispiele:

- [Die AppCompatActivity klasse ShowShaked](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/java/li/tor/droid/shakeit/ShowShaked.java)
- [Das Interface AccelerometerListener](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/java/li/tor/droid/shakeit/AccelerometerListener.java)
- [Die Ganze Klasse - AccelerometerManager](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/java/li/tor/droid/shakeit/AccelerometerManager.java)
- [Die Manifest.xml](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/AndroidManifest.xml)
- [Die view "activity_show_shaked.xml"](https://gitlab.com/casaper/shake_detect_demo/blob/master/app/src/main/res/layout/activity_show_shaked.xml)



Oder gleich das ganze Git-Repository: 

- [https://gitlab.com/casaper/shake_detect_demo](https://gitlab.com/casaper/shake_detect_demo)
- `git@gitlab.com:casaper/shake_detect_demo.git`



Frei nach https://www.spaceotechnologies.com/integrate-android-accelerometer-detect-shake/

